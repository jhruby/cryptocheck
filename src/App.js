import React, {Component} from "react";
import {AppState, PushNotificationIOS} from "react-native";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import PushNotification from "react-native-push-notification";
import {store, persistor} from "./store";
import Navigation from "./screens";
import {fetchPrices} from "./actions/priceActions";

PushNotification.configure({
    onNotification: notification => {
        notification.finish(PushNotificationIOS.FetchResult.NoData);
    },
});

export default class App extends Component {
    componentDidMount() {
        store.dispatch(fetchPrices());
        AppState.addEventListener("change", this.handleAppStateChange);
    }

    componentWillUnmount() {
        AppState.removeEventListener("change", this.handleAppStateChange);
    }

    handleAppStateChange = nextAppState => {
        if (nextAppState === "active") {
            store.dispatch(fetchPrices());
        }
    };

    render() {
        return (
            <Provider store={store}>
                <PersistGate persistor={persistor} loading={null}>
                    <Navigation />
                </PersistGate>
            </Provider>
        );
    }
}
