export const colorMappings = {
    BTC: ["#4776E6", "#8E54E9"],
    DASH: ["#ff9a44", "#fc6076"],
    ETH: ["#8ddad5", "#00cdac"],
    LTC: ["#f6d365", "#fda085"],
    XRP: ["#48c6ef", "#6f86d6"],
    BCH: ["#ec8c69", "#ed6ea0"],
    XLM: ["#72cee3", "#17acaa"],
    default: ["#868f96", "#596164"],
};
