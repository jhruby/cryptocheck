import actions from "../actions/actionConstants";

const randomBalance = () => ({
    BTC: Math.random(),
    BCH: Math.random() * 10,
    ETH: Math.random() * 10,
    LTC: Math.random() * 10,
    XRP: Math.random() * 1000,
    DASH: Math.random() * 10,
});

const myBalance = {
    BTC: 0.8,
    BCH: 0.1,
    ETH: 0.5,
    LTC: 7,
    XRP: 293.4,
    DASH: 0.232582,
    XLM: 807.3,
};

const initialState = {
    balance: myBalance,
    currency: "EUR",
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.SET_BALANCE:
            return {
                ...state,
                balance: action.payload,
            };

        case actions.SET_RANDOM_BALANCE:
            return {
                ...state,
                balance: randomBalance(),
            };

        case actions.UPDATE_BALANCE:
            return {
                ...state,
                balance: {
                    ...state.balance,
                    [action.payload.abbr]: action.payload.value,
                },
            };

        default:
            return state;
    }
};

export default userReducer;
