import {combineReducers} from "redux";
import priceReducer from "./priceReducer";
import userReducer from "./userReducer";

export default combineReducers({
    priceState: priceReducer,
    userState: userReducer,
});
