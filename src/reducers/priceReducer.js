import actions from "../actions/actionConstants";

const initialState = {
    prices: undefined,
};

const priceReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.SAVE_FETCHED_PRICES:
            return {
                ...state,
                prices: action.payload,
            };

        default:
            return state;
    }
};

export default priceReducer;
