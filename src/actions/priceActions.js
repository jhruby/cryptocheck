import actions from "./actionConstants";

export const fetchPrices = () => async (dispatch, getState) => {
    try {
        const {currency} = getState().userState;
        const res = await fetch(
            `https://api.coinmarketcap.com/v1/ticker/?convert=${currency}&limit=20`
        );
        const prices = await res.json();
        await dispatch(saveFetchedPrices(prices));
    } catch (e) {
        console.error(e);
    }
};

const saveFetchedPrices = prices => ({
    type: actions.SAVE_FETCHED_PRICES,
    payload: prices,
});
