import actions from "./actionConstants";

export const setBalance = balance => ({
    type: actions.SET_BALANCE,
    payload: balance,
});

export const setRandomBalance = () => ({
    type: actions.SET_RANDOM_BALANCE,
});

export const updateBalance = (abbr, value) => ({
    type: actions.UPDATE_BALANCE,
    payload: {
        abbr,
        value,
    },
});
