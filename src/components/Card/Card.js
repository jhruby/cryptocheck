import React, {Component} from "react";
import {View, Text} from "react-native";
import PropTypes from "prop-types";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import {colorMappings} from "../../config";
import {formatCurrency, formatNumber} from "../../utils";

export default class Card extends Component {
    static propTypes = {
        name: PropTypes.string,
        abbr: PropTypes.string,
        price: PropTypes.number,
        last24h: PropTypes.number,
        balance: PropTypes.number,
        currency: PropTypes.string,
    };

    render() {
        const {name, abbr, price, last24h, balance, currency} = this.props;
        const balanceReal = price * balance;

        return (
            <LinearGradient
                colors={colorMappings[abbr] || colorMappings.default}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 1}}
                style={styles.card}>
                <View style={[styles.row, {marginBottom: 12}]}>
                    <Text style={styles.name}>{name}</Text>
                    <Text style={styles.price}>
                        {formatCurrency(price, currency)}
                    </Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.balance}>
                        <Text style={{opacity: 0.7}}>
                            {formatNumber(balance)} {abbr} ={" "}
                        </Text>
                        {formatCurrency(balanceReal, currency)}
                    </Text>
                    <Text style={[styles.balance, {opacity: 0.85}]}>
                        {formatNumber(last24h)} %
                    </Text>
                </View>
            </LinearGradient>
        );
    }
}
