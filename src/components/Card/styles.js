import {StyleSheet} from "react-native";

export default StyleSheet.create({
    card: {
        backgroundColor: "#fff",
        marginBottom: 16,
        padding: 16,
        borderRadius: 8,
    },

    row: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },

    name: {
        color: "#fff",
        fontSize: 25,
        fontWeight: "600",
    },

    price: {
        color: "#fff",
        fontSize: 22,
        fontWeight: "400",
    },

    balance: {
        color: "#fff",
        fontSize: 20,
        fontWeight: "300",
    },
});
