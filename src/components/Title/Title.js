import React from "react";
import {Text} from "react-native";
import PropTypes from "prop-types";

const Title = ({children}) => (
    <Text
        style={{
            textAlign: "center",
            color: "#3e3e3e",
            fontSize: 44,
            fontWeight: "800",
            marginTop: 8,
            marginBottom: 8,
        }}>
        {children}
    </Text>
);

Title.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Title;
