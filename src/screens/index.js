import {TabNavigator} from "react-navigation";
import OverviewScreen from "./Overview";

export default TabNavigator(
    {
        OverviewScreen,
    },
    {
        navigationOptions: {
            tabBarVisible: false,
        },
    }
);
