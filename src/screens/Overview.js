import React, {Component} from "react";
import {ScrollView, View, RefreshControl, StatusBar} from "react-native";
import PropTypes from "prop-types";
import PushNotification from "react-native-push-notification";
import BackgroundFetch from "react-native-background-fetch";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Card from "../components/Card";
import Title from "../components/Title";
import {fetchPrices} from "../actions/priceActions";
import {setRandomBalance} from "../actions/userActions";
import {formatCurrency} from "../utils";

class OverviewScreen extends Component {
    static propTypes = {
        prices: PropTypes.array,
        currency: PropTypes.string,
        balance: PropTypes.objectOf(PropTypes.number),
        fetchPrices: PropTypes.func,
    };

    state = {
        prices: null,
        refreshing: false,
    };

    componentDidMount() {
        const {currency, fetchPrices} = this.props;
        BackgroundFetch.configure(
            {},
            async () => {
                await fetchPrices();
                PushNotification.localNotification({
                    message: formatCurrency(this.calculateTotal(), currency),
                });
                BackgroundFetch.finish();
            },
            error => {
                console.error(error);
            }
        );
    }

    onRefresh = async () => {
        const {fetchPrices} = this.props;
        this.setState({refreshing: true});
        await fetchPrices();
        this.setState({refreshing: false});
    };

    calculateTotal() {
        const {prices, balance, currency} = this.props;
        const total = prices.reduce((sum, cur) => {
            if (Object.keys(balance).includes(cur.symbol)) {
                return (
                    sum +
                    Number(cur[`price_${currency.toLowerCase()}`]) *
                        balance[cur.symbol]
                );
            } else {
                return sum;
            }
        }, 0);
        return total;
    }

    renderCards() {
        const {prices, balance, currency} = this.props;
        return Object.keys(balance).map(symbol => {
            const coin = prices.find(coin => coin.symbol === symbol);
            return (
                coin && (
                    <Card
                        key={symbol}
                        name={coin.name}
                        abbr={coin.symbol}
                        price={Number(coin[`price_${currency.toLowerCase()}`])}
                        last24h={Number(coin.percent_change_24h)}
                        balance={balance[symbol]}
                        currency={currency}
                    />
                )
            );
        });
    }

    render() {
        const {refreshing} = this.state;
        const {prices, currency} = this.props;
        if (!prices) {
            return null;
        }

        return (
            <ScrollView
                style={{marginTop: 20}}
                contentContainerStyle={{padding: 12}}
                stickyHeaderIndices={[1]}
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={this.onRefresh}
                    />
                }>
                <StatusBar barStyle="dark-content" />
                <View style={{backgroundColor: "#fff"}}>
                    <Title>
                        {formatCurrency(this.calculateTotal(), currency)}
                    </Title>
                </View>

                {this.renderCards()}
            </ScrollView>
        );
    }
}

export default connect(
    state => ({
        prices: state.priceState.prices,
        currency: state.userState.currency,
        balance: state.userState.balance,
    }),
    dispatch => bindActionCreators({fetchPrices, setRandomBalance}, dispatch)
)(OverviewScreen);
