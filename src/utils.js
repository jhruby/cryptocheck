export const formatCurrency = (number, currency = "EUR") =>
    number.toLocaleString("cs-CZ", {
        style: "currency",
        currency: currency,
        minimumFractionDigits: 0,
        maximumFractionDigits: number > 100 ? 0 : number > 10 ? 1 : 2,
    });

export const formatNumber = number =>
    number.toLocaleString("cs-CZ", {
        minimumFractionDigits: 1,
        maximumFractionDigits: 1,
    });
