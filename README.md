# Cryptocheck

Quickly check your current balance of cryptoassets. See current price in EUR as well as daily change.

![Screenshot](screenshot.png)